# mb-solos-pilot

Mapeamento espaço-temporal de propriedades do solo no Brasil

_Alessandro Samuel-Rosa_

Proposta de mapeamento espaço-temporal de propriedades do solo no Brasil integrado às iniciativas multi-institucionais MapBiomas e PronaSolos. A proposta é baseada em estudo de caso de mapeamento espaço-temporal do conteúdo de carbono na camada superficial do solo de áreas agrícolas em municípios do Alto Uruguai, Rio Grande do Sul.
